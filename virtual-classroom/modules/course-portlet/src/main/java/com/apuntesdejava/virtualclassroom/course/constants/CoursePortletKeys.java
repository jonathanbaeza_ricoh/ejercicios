package com.apuntesdejava.virtualclassroom.course.constants;

/**
 * @author Daniel.Blanchart
 */
public class CoursePortletKeys {

	public static final String COURSE =
		"CLASSROOM_COURSE";

}